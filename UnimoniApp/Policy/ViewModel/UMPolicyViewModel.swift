//
//  UMPolicyViewModel.swift
//  UnimoniApp
//
//  Created by Nikhil Nandha on 17/03/19.
//  Copyright © 2019 Nikhil Nandha. All rights reserved.
//

import UIKit

class UMPolicyViewModel: NSObject {

    static let shared = UMPolicyViewModel()
    
    private var policies : UMPolicyModel?
    
    func getPolicies() -> UMPolicyModel {
        if policies == nil {
            return UMPolicyModel(responseStatus:AnyCodable.init(value: 0), plans: [])
        }else {
            return policies!
        }
    }
    
    func setPolicies(_ data: UMPolicyModel) {
        policies = data
    }
    
    func getImageNameFor(name: String) -> String {
        
        switch name {
        case "IHO_MOBILE_ICON":
            return "tele_consult.png"
        case "IHO_CHECKUP_ICON":
            return "health_check.png"
        case "IHO_PHARMACY_ICON":
            return "medicine.png"
        case "IHO_DENTAL_ICON":
            return "dental_check.png"
        case "IHO_DISCOUNT_CARD_ICON":
            return "discount_card.png"
        default:
            return ""
        }
        
    }
}
