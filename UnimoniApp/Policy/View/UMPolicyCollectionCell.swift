//
//  UMPolicyCollectionCell.swift
//  UnimoniApp
//
//  Created by Nikhil Nandha on 17/03/19.
//  Copyright © 2019 Nikhil Nandha. All rights reserved.
//

import UIKit

class UMPolicyCollectionCell: UICollectionViewCell {

    @IBOutlet var imageV: UIImageView!
    @IBOutlet var label_title: UILabel!
    @IBOutlet var label_detail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
