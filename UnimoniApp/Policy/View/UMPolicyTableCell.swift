//
//  UMPolicyTableCell.swift
//  UnimoniApp
//
//  Created by Nikhil Nandha on 17/03/19.
//  Copyright © 2019 Nikhil Nandha. All rights reserved.
//

import UIKit

class UMPolicyTableCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var label_plan: UILabel!
    @IBOutlet var label_rpice: UILabel!
    
    @IBOutlet var label_buyNow: UILabel!
    @IBOutlet var button_buyNow: UIButton!
    
    @IBOutlet var collectionV: UICollectionView!
    
    @IBOutlet var view_container: UIView!
    
    var row : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        UIManager.shared.addShadowToView(view_container)
        collectionV.register(UINib.init(nibName: "UMPolicyCollectionCell", bundle: nil), forCellWithReuseIdentifier: "UMPolicyCollectionCell")
        collectionV.register(UINib.init(nibName: "UMPolicyAddCollectionCell", bundle: nil), forCellWithReuseIdentifier: "UMPolicyAddCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataForRow(rowIndex: Int) {
        row = rowIndex
        
        let plan: PlanModel = UMPolicyViewModel.shared.getPolicies().plans[row!]
        
        label_plan.text = plan.plan_name?.value as? String
        label_rpice.text = "₹ \(plan.plan_cost?.value as! Int)"
        
        label_buyNow.text = "BUY NOW FOR \(plan.plan_disc_cost?.value as! Int)"
        
        collectionV.delegate = self
        collectionV.dataSource = self
        collectionV.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let plan: PlanModel = UMPolicyViewModel.shared.getPolicies().plans[row!]
        return plan.benefits.count+1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let plan: PlanModel = UMPolicyViewModel.shared.getPolicies().plans[row!]
        
        if indexPath.item == plan.benefits.count {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UMPolicyAddCollectionCell", for: indexPath) as! UMPolicyAddCollectionCell
            
            cell.imageV.image = UIImage.init(named: "save_badge.png")
            
            return cell
            
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UMPolicyCollectionCell", for: indexPath) as! UMPolicyCollectionCell
            
            
            let benefit: Benefits = plan.benefits[indexPath.item]
            let imageName = benefit.benefit_icon_local?.value as! String
            
            cell.imageV.image = UIImage.init(named: UMPolicyViewModel.shared.getImageNameFor(name: imageName))
            cell.label_title.text = benefit.benefit_title?.value as? String
            cell.label_detail.text = benefit.benefit_sub_title?.value as? String
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: (collectionView.frame.size.width/2)-2, height: 156.0)
        
    }
    
}
