//
//  UMPolicyModel.swift
//  UnimoniApp
//
//  Created by Nikhil Nandha on 17/03/19.
//  Copyright © 2019 Nikhil Nandha. All rights reserved.
//

import UIKit

struct UMPolicyModel : Codable {
    
    var responseStatus: AnyCodable?
    var plans : [PlanModel]
    
}

struct PlanModel : Codable {
    
    var plan_name : AnyCodable?
    var plan_cost : AnyCodable?
    var plan_disc_cost : AnyCodable?
    var plan_save_upto : AnyCodable?
    
    var benefits : [Benefits]
    
}

struct Benefits : Codable {
    var benefit_title : AnyCodable?
    var benefit_sub_title : AnyCodable?
    var benefit_icon_network: AnyCodable?
    var benefit_icon_local: AnyCodable?
}

struct RequestModel : Codable{
    var data: AnyCodable
}
    
