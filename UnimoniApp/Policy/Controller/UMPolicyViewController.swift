//
//  UMPolicyViewController.swift
//  UnimoniApp
//
//  Created by Nikhil Nandha on 17/03/19.
//  Copyright © 2019 Nikhil Nandha. All rights reserved.
//

import UIKit

class UMPolicyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableV : UITableView!
    @IBOutlet var view_button : UIView!
    @IBOutlet var view_button_indicator : UIView!
    
    @IBOutlet var button_two_member : UIButton!
    @IBOutlet var button_four_member : UIButton!
    
    var collectionCellHeight = 188.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view_button_indicator.layer.cornerRadius = view_button_indicator.frame.height/2
        view_button_indicator.clipsToBounds = true
        view_button.layer.cornerRadius = view_button.frame.height/2
        view_button.clipsToBounds = true
        
        tableV.register(UINib.init(nibName: "UMPolicyTableCell", bundle: nil), forCellReuseIdentifier: "UMPolicyTableCell")
        tableV.delegate = self;
        tableV.dataSource = self;
        
        self.title = "Member Selection"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        plotScreen()
    }
    
    func reloadTable() {
        tableV.delegate = self
        tableV.dataSource = self
        tableV.reloadData()
    }
    
    func plotScreen() {
        selectTwoMember(true)
    }
    
    
    
    //Mark: Table View Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UMPolicyViewModel.shared.getPolicies().plans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UMPolicyTableCell", for: indexPath) as! UMPolicyTableCell
        
        cell.setDataForRow(rowIndex: indexPath.row)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let plan: PlanModel = UMPolicyViewModel.shared.getPolicies().plans[indexPath.row]
        var benefitsCount: Int = 0
        
        if plan.benefits.count%2 == 1{
            benefitsCount = (plan.benefits.count/2)+1
        }else {
            benefitsCount = (plan.benefits.count/2)
        }
        
        return CGFloat(112+(benefitsCount*156))
    }
    
    
    
    //Mark: Button Tapped Methods
    
    @IBAction func twoMemberTapped() {
        button_two_member.isSelected = true
        button_four_member.isSelected = false
        selectTwoMember(true)
    }
    
    @IBAction func fourMemberTapped() {
        button_four_member.isSelected = true
        button_two_member.isSelected = false
        selectTwoMember(false)
    }
    
    func selectTwoMember(_ selectTwo: Bool) {
        
        var selectViewOrigin = 0
        
        if selectTwo {
            selectViewOrigin = Int(button_two_member.frame.origin.x)
            button_two_member.isSelected = true
        }
        else {
            selectViewOrigin = Int(button_four_member.frame.origin.x)
            button_four_member.isSelected = true
        }
        
        var frame = self.view_button_indicator.frame
        frame.origin.x = CGFloat(selectViewOrigin)
        frame.size.width = self.button_four_member.frame.width
        
        UIView.animate(withDuration: 0.75) {
            
            self.view_button_indicator.frame = frame
        }
        
        callApi()
    }
    
    //Mark: Web Services Methods

    func callApi() {
        
        var memberCount = ""
        if button_two_member.isSelected {
            memberCount = "2"
        }
        else if button_four_member.isSelected {
            memberCount = "4"
        }
        
        let dataModel = RequestModel.init(data: AnyCodable.init(value: ""))
        
        UIManager.shared.showActivityIndicator(_parentView: self.view, _backgroundClickable: false, _title: "Loading...")
        
        WebServiceManager.sharedInstance.callWebService(requestType: APIRequestType.APIRequestTypeGet, methodName: "member_count=\(memberCount)", data: dataModel) { (response) in
            
            DispatchQueue.main.async {
                
                UIManager.shared.hideActivityIndicator()
                
                if response != nil {
                    
                    print(response as Any)
                    
                    do {
                        
                        let jsonDecoder = JSONDecoder()
                        let policies : UMPolicyModel = try jsonDecoder.decode(UMPolicyModel.self,
                                                                              from: JSONSerialization.data(withJSONObject: (response as! Dictionary<String, Any>), options: []))
                        
                        UMPolicyViewModel.shared.setPolicies(policies)
                        self.reloadTable()
                        
                    }
                    catch _ {
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
