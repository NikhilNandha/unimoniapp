//
//  WebServiceManager.swift
//  SwiftTest
//
//  Created by Nikhil Nandha on 01/08/18.
//  Copyright © 2018 Nikhil Nandha. All rights reserved.
//

import UIKit

enum APIRequestType {
    case APIRequestTypePost
    case APIRequestTypeGet
}

class WebServiceManager {

    static let sharedInstance = WebServiceManager()
    
    func callWebService<CodableProtocol>(requestType: APIRequestType, methodName : String, data : CodableProtocol, completion:@escaping ((Any)?) -> Void) where CodableProtocol : Codable
    {
        let urlString = "http://demo4929648.mockable.io/IHOPolicyDetails?" + methodName as String
        var dataDictToSend : Any?
        
        //Create Reqeust Body Params
        do {
            let jsonEncoder = JSONEncoder()
            let dataToSend : Data = try jsonEncoder.encode(data)
            
            dataDictToSend = try! JSONSerialization.jsonObject(with: dataToSend, options: [])
            
        }
        catch {}
        
        if (requestType == .APIRequestTypePost && dataDictToSend != nil)
        {
            api(type: requestType, urlString: urlString, data: dataDictToSend!, completion: completion)
        }
        else {
            if requestType == .APIRequestTypePost {  //For POST Type Data SHOULD NOT BE NULL
            
            }else {
                api(type: requestType, urlString: urlString, data: nil, completion: completion)
            }
        }
    }
    
    
    
    private func api(type: APIRequestType, urlString : String, data : Any?, completion:@escaping ((Any)?) -> Void) {
        
        //Create Reqeust
        var request : URLRequest = URLRequest(url: URL(string: urlString)!)
        if type == .APIRequestTypeGet {
            request.httpMethod = "GET"
        }else {
            request.httpMethod = "POST"
        }
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        
        print("\(urlString)\n\(String(describing: data))")
        
        if (type == .APIRequestTypePost && data != nil) {
            guard let body = try? JSONSerialization.data(withJSONObject: data as! Dictionary<String, Any>, options: []) else { return }
            
            request.httpBody = body
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request)
        {
            (data , response, error) in
            
            //Handle API Response
            self.handleResponse(data: data, response: response, error: error, completion: completion)
            
        }
        
        task.resume()
    }
    
    
    private func handleResponse(data : Data?, response : URLResponse?, error : Error?, completion:@escaping ((Any)?) -> Void) {
        
        //Check if error then return
        if error == nil {
            guard let data = data else { //Check if data is nil then return
                completion(nil)
                return
            }
            
            guard let jsonResponse = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else { //Check if json is not valid then return
                completion(nil)
                return
            }
            
            if  jsonResponse is Dictionary<String, Any> {
                var jsonResponseData = jsonResponse as? Dictionary<String, Any>
                if jsonResponseData!["responseStatus"] as! Bool == true {  //Check if success
                    
                    completion(jsonResponseData)
                    
                }else {
                    completion(nil)
                }
            }
            else if jsonResponse is Array<Any> {
                completion(jsonResponse)
            }
            
            
        }else {
            completion(nil)
        }
        
    }
    
    
    private func shouldAddDeviceInfoToRequestData(methodName : String) -> Bool {
        
        let methodsArray_notToAddDeviceInfo = [""]
        
        if methodsArray_notToAddDeviceInfo.contains(methodName) {
            return false
        }
        
        return true
    }
    
//    func getTypesOfProperties(in clazz: NSObject.Type) -> Dictionary<String, Any>? {
//        var count = UInt32()
//        guard let properties = class_copyPropertyList(clazz, &count) else { return nil }
//        var types: Dictionary<String, Any> = [:]
//        for i in 0..<Int(count) {
//            guard
//                let property: objc_property_t = properties[i],
//                let name = getNameOf(property: property)
//                else { continue }
//            let type = getTypeOf(property: property)
//            types[name] = type
//        }
//        free(properties)
//        return types
//    }
//
//    func getNameOf(property: objc_property_t) -> String? {
//        guard
//            let name: NSString = NSString(utf8String: property_getName(property))
//            else { return nil }
//        return name as String
//    }
//
//    func getTypeOf(property: objc_property_t) -> Any {
//        guard let attributesAsNSString: NSString = NSString(utf8String: property_getAttributes(property)!) else { return Any.self }
//        let attributes = attributesAsNSString as String
//        let slices = attributes.components(separatedBy: "\"")
//        guard slices.count > 1 else { return valueType(withAttributes: attributes) }
//        let objectClassName = slices[1]
//        let objectClass = NSClassFromString(objectClassName) as! NSObject.Type
//        return objectClass
//    }
//
//    let valueTypesMap: Dictionary<String, Any> = [
//        "c" : Int8.self,
//        "s" : Int16.self,
//        "i" : Int32.self,
//        "q" : Int.self, //also: Int64, NSInteger, only true on 64 bit platforms
//        "S" : UInt16.self,
//        "I" : UInt32.self,
//        "Q" : UInt.self, //also UInt64, only true on 64 bit platforms
//        "B" : Bool.self,
//        "d" : Double.self,
//        "f" : Float.self,
//        "{" : Decimal.self
//    ]
//
//    func valueType(withAttributes attributes: String) -> Any {
//        guard let letter : String = "\(attributes[attributes.index(attributes.startIndex, offsetBy: 1)])",
//            let type = valueTypesMap[letter] else { return Any.self }
//        return type
//    }
    
}
