//
//  AppConstants.swift
//  Swift1
//
//  Created by Nikhil Nandha on 12/11/18.
//  Copyright © 2018 Nikhil Nandha. All rights reserved.
//

import UIKit

struct AppConstantsSW {
    
    static let SCREEN_WIDTH:CGFloat = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT:CGFloat = UIScreen.main.bounds.size.height
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
}
