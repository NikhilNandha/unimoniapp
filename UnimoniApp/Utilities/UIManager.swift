//
//  UIManager.swift
//  UnimoniApp
//
//  Created by Nikhil Nandha on 18/03/19.
//  Copyright © 2019 Nikhil Nandha. All rights reserved.
//

import Foundation
import UIKit

class UIManager {
    
    static let shared = UIManager()
    
    var currentView: AnyObject?
    var activityIndicator:UIActivityIndicatorView?
    var activityIndicatorBG:UIView?
    var activityIndicatorTitle:UILabel?
    
    
    //////////////////////////////
    //MARK: Activity Indicator Method
    //////////////////////////////
    
    
    func showActivityIndicator(_parentView: UIView, _backgroundClickable: Bool = false, _title: String = "")
    {
        if activityIndicatorBG != nil
        {
            hideActivityIndicator()
        }
        
        if activityIndicatorBG == nil{
            activityIndicatorBG = UIView(frame: CGRect(x: 0, y: 0, width: _parentView.frame.size.width, height: _parentView.frame.size.height))
            activityIndicatorBG?.backgroundColor = UIColor.black
            activityIndicatorBG?.alpha = 0.65
            _parentView.addSubview(activityIndicatorBG!)
            
            activityIndicatorBG?.isUserInteractionEnabled = !_backgroundClickable
        }
        
        if activityIndicator == nil{
            activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
            activityIndicator?.center = CGPoint(x: _parentView.frame.size.width/2, y: _parentView.frame.size.height/2.3)
            activityIndicator?.startAnimating()
            _parentView.addSubview(activityIndicator!)
        }
        
        activityIndicatorTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 100))
        activityIndicatorTitle?.text = _title
        activityIndicatorTitle?.font = UIFont.systemFont(ofSize: 17)
        activityIndicatorTitle?.textColor = UIColor.white
        activityIndicatorTitle?.alpha = 0.9
        
        activityIndicatorTitle?.numberOfLines = 2
        activityIndicatorTitle?.lineBreakMode = NSLineBreakMode.byWordWrapping
        activityIndicatorTitle?.sizeToFit()
        activityIndicatorTitle?.center = CGPoint(x: _parentView.frame.size.width / 2 , y: (activityIndicator?.frame.origin.y)! + (activityIndicator?.frame.size.height)! * 1.8)
        activityIndicatorTitle?.textAlignment = .center
        
        _parentView.addSubview(activityIndicatorTitle!)
    }
    
    func hideActivityIndicator()
    {
        if activityIndicatorBG != nil{
            
            DispatchQueue.main.async {
                self.activityIndicatorBG?.removeFromSuperview()
                self.activityIndicatorBG = nil
            }
            
        }
        
        if activityIndicator != nil{
            DispatchQueue.main.async {
                
                self.activityIndicator?.removeFromSuperview()
                self.activityIndicator = nil
            }
            
        }
        
        if activityIndicatorTitle != nil{
            DispatchQueue.main.async {
                
                self.activityIndicatorTitle?.removeFromSuperview()
                self.activityIndicatorTitle = nil
            }
            
        }
    }
    
    func addShadowToView(_ view:UIView)
    {
        view.layer.shadowColor =  UIColor.gray.cgColor;
        view.layer.shadowOpacity = 0.3;
        view.layer.shadowRadius = 2;
        view.layer.shadowOffset = CGSize(width: 0, height: 0);
    }
    
}
